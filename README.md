# Meme generator ELG example

This project is a wrapper around (a patched version of) https://github.com/nomad-software/meme, to serve as an example of a [European Language Grid](https://www.european-language-grid.eu) LT service that makes use of the temporary storage facility to return data that cannot be easily represented in any of the normal ELG JSON message formats.

## API

The service exposes an endpoint on `http://<IP>:8080/meme` which expects an ELG-standard "text" request

```json
{
  "type": "text",
  "content": "Text to be\noverlaid",
  "params": {
    "image": "image-name"
  }
}
```

or "structuredText" request:

```json
{
  "type": "structuredText",
  "texts": [
    {"content": "Text for the top"},
    {"content": "Text for the bottom"}
  ],
  "params": {
    "image": "image-name"
  }
}
```

In the "text" case the "content" should be one or two lines of text separated by `\n`, the first (or only) line will be used as the "top" text of the meme and the second (if present) will be the "bottom" text.  The "image" parameter should be one of the built in image names from https://github.com/nomad-software/meme/tree/master/data/images (without the `.jpg` extension).

The service merges the text and the image to generate a meme, posts the image to the well-known URL `http://storage.elg/store`, which points to the temporary storage endpoint within an ELG cluster, and returns an ELG "annotations" response including the URL from which the meme can be downloaded:

```json
{
  "response": {
    "type": "annotations",
    "annotations": {
      "Top": [{"start": 0, "end": 10}],
      "Bottom": [{"start": 11, "end": 21}]
    },
    "features": {
      "imageUrl": "<download-url-of-generated-image>"
    }
  }
}
```

The Top and Bottom annotations mark which parts of the input text were used as the top and bottom meme texts, and the "imageUrl" feature is the link from which the image may be downloaded.