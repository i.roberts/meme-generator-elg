module gitlab.com/i.roberts/meme-generator-elg

go 1.16

require github.com/nomad-software/meme v1.0.1

replace github.com/nomad-software/meme => github.com/ianroberts/meme v1.0.1-ir
