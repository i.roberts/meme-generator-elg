FROM golang:1.16-alpine AS builder

WORKDIR /tmp/go-build

COPY go.mod go.sum ./
RUN go mod download

COPY . .
RUN go build -o ./meme-generator-elg .

FROM alpine:3.13

# Add ca-certificates
RUN apk update && \
   apk add ca-certificates && \
   update-ca-certificates && \
   rm -rf /var/cache/apk/*

COPY --from=builder /tmp/go-build/meme-generator-elg /meme-generator-elg
ENTRYPOINT ["/meme-generator-elg"]

