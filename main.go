package main

import (
	"encoding/json"
	"fmt"
	"github.com/nomad-software/meme/cli"
	"github.com/nomad-software/meme/image"
	"github.com/nomad-software/meme/image/stream"
	"gitlab.com/i.roberts/meme-generator-elg/elg"
	_ "image/gif"
	_ "image/jpeg"
	_ "image/png"
	"mime"
	"net/http"
	"sort"
	"strings"
	"unicode/utf8"
)

func main() {
	http.HandleFunc("/meme", MakeMeme)
	http.HandleFunc("/healthz", func(w http.ResponseWriter, r *http.Request) {
		_, _ = fmt.Fprint(w, "OK")
	})
	http.ListenAndServe(":8080", nil)
}

func MakeMeme(w http.ResponseWriter, r *http.Request) {
	mimeType, _, err := mime.ParseMediaType(r.Header.Get("Content-Type"))
	if err != nil || mimeType != "application/json" {
		failureResponse(w, r, 400, "elg.request.invalid", "Invalid request")
		return
	}

	decoder := json.NewDecoder(r.Body)
	req := elg.TextOrStructuredRequest{}
	if err := decoder.Decode(&req); err != nil || (len(req.Content) == 0 && len(req.Texts) == 0) {
		failureResponse(w, r, 400, "elg.request.invalid", "Invalid request")
		return
	}

	imageName := req.Params["image"]
	if len(imageName) == 0 {
		failureResponse(w, r, 400, "elg.param.missing", "No value specified for parameter {0}", "image")
		return
	}
	idx := sort.Search(len(cli.ImageIds), func(i int) bool { return cli.ImageIds[i] >= imageName })
	if cli.ImageIds[idx] != imageName {
		failureResponse(w, r, 400, "elg.param.invalid", "Value \"{0}\" invalid for parameter {1}", imageName, "image")
		return
	}

	var topText, bottomText string
	anns := make(map[string][]elg.Annotation)
	if len(req.Texts) > 0 {
		// structured request
		topText = req.Texts[0].Content
		anns["Top"] = []elg.Annotation{
			{
				Start: 0,
				End:   1,
			},
		}
		if len(req.Texts) > 1 {
			bottomText = req.Texts[1].Content
			anns["Bottom"] = []elg.Annotation{
				{
					Start: 1,
					End:   2,
				},
			}
		}
	} else {
		lines := strings.SplitN(req.Content, "\n", 2)
		topText = lines[0]
		topEnd := int64(utf8.RuneCountInString(topText))
		anns["Top"] = []elg.Annotation{
			{
				Start: 0,
				End:   topEnd,
			},
		}
		if len(lines) > 1 && len(lines[1]) > 0 {
			bottomText = lines[1]
			anns["Bottom"] = []elg.Annotation{
				{
					Start: topEnd + 1,
					End:   topEnd + 1 + int64(utf8.RuneCountInString(bottomText)),
				},
			}

		}
	}

	opts := cli.Options{
		Top:   topText,
		Image: imageName,
	}
	if len(bottomText) > 0 {
		opts.Bottom = bottomText
	}

	var img stream.Stream
	var renderErr string
	func() {
		// RenderImage can panic, we want to convert that to a sensible error
		defer func() {
			if r := recover(); r != nil {
				renderErr = r.(string)
			}
		}()
		img = image.Load(opts)
		img = image.RenderImage(opts, img)
	}()

	if len(renderErr) > 0 {
		failureResponse(w, r, 500, "elg.service.internalError", "Internal error during processing: {0}", renderErr)
		return
	}

	var contentType string
	switch {
	case img.IsGif():
		contentType = "image/gif"
	case img.IsJpg():
		contentType = "image/jpeg"
	default:
		contentType = "image/png"
	}
	postResp, err := http.Post("http://storage.elg/store", contentType, &img)
	if err != nil {
		failureResponse(w, r, 500, "elg.service.internalError", "Internal error during processing: {0}", err.Error())
		return
	}
	decoder = json.NewDecoder(postResp.Body)
	uploadResponse := elg.StoredResponseMessage{}
	if err := decoder.Decode(&uploadResponse); err != nil {
		failureResponse(w, r, 500, "elg.service.internalError", "Internal error during processing: {0}", err.Error())
		return
	}
	w.Header().Set("Content-Type", "application/json")
	if uploadResponse.Failure != nil {
		w.WriteHeader(500)
		encoder := json.NewEncoder(w)
		encoder.Encode(uploadResponse)
	} else {
		featureMap := make(map[string]string)
		featureMap["imageUrl"] = uploadResponse.Response.URI
		w.WriteHeader(200)
		encoder := json.NewEncoder(w)
		encoder.Encode(&elg.AnnotationsResponseMessage{
			Response: &elg.AnnotationsResponse{
				Type:        "annotations",
				Annotations: anns,
				Features:    featureMap,
			},
		})
	}
}

func failureResponse(w http.ResponseWriter, r *http.Request, responseCode int, code, text string, params ...string) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(responseCode)
	enc := json.NewEncoder(w)
	_ = enc.Encode(elg.AnnotationsResponseMessage{
		Failure: &elg.Failure{
			Errors: []elg.StatusMessage{
				{
					Code:   code,
					Text:   text,
					Params: params,
				},
			},
		},
	})
}
