define("elg/common", ["jquery", "mdc"], function ($, mdc) {

    return (function () {
        function ElgCommon(readyCallback, afterErrorCallback, submitProgress) {
            var this_ = this;
            this.injectedCss = false;
            this.fetchedParams = false;
            this.serviceInfo = {ServiceUrl: null, Authorization: null};
            this.afterErrorCallback = afterErrorCallback;
            this.submitProgress = submitProgress;
            this.collator = function(a, b) { return a.localeCompare(b); }

            // Listen to messages from parent window
            window.addEventListener('message', function (e) {
                if ((window.location.origin === e.origin) && e.data != '') {
                    this_.serviceInfo = JSON.parse(e.data);
                    if (this_.serviceInfo.ServiceUrl.indexOf('async/') >= 0) {
                        this_.i18nUrl = new URL('../../../i18n/resolve', this_.serviceInfo.ServiceUrl);
                    } else {
                        this_.i18nUrl = new URL('../../i18n/resolve', this_.serviceInfo.ServiceUrl);
                    }
                    if (this_.serviceInfo.Language) {
                        i18nUrl.searchParams.append('lang', this_.serviceInfo.Language);
                    } else {
                        // store language away for future use
                        this_.serviceInfo.Language = navigator.language.split(/-/, 2)[0];
                    }
                    this_.collator = new Intl.Collator(this_.serviceInfo.Language).compare;
                    if (!this_.injectedCss) {
                        // inject CSS
                        var elgCss = $('<link type="text/css" rel="stylesheet" media="screen,print">');
                        elgCss.attr('href', this_.serviceInfo.StyleCss);
                        $('head').append(elgCss);
                        this_.injectedCss = true;
                    }
                    if (!this_.fetchedParams) {
                        this_.fetchParams(readyCallback);
                    }
                }
            });
            // and tell the parent we're ready for a message
            setTimeout(function () {
                window.parent.postMessage("GUI:Ready for config", window.location.origin);
            }, 500);
        }

        ElgCommon.prototype.withAuthSettings = function (obj) {
            if (this.serviceInfo.Authorization) {
                obj.xhrFields = {withCredentials: true};
                obj.headers = {Authorization: this.serviceInfo.Authorization};
            }
            return obj;
        };

        ElgCommon.prototype.buildParamsForm = function(params) {
            var this_ = this;

            function i(msg) {
                // return a suitable entry from an object keyed by language code
                // - first preference - user's configured language
                // - second preference - English
                // - fallback - first language in iteration order (e.g. if the object
                //   only has one language in it and that language isn't English)
                if(msg.hasOwnProperty(this_.serviceInfo.Language)) {
                    return msg[this_.serviceInfo.Language];
                } else if(msg.hasOwnProperty('en')) {
                    return msg.en;
                } else {
                    for (p in msg) {
                        if(msg.hasOwnProperty(p)) {
                            return msg[p];
                        }
                    }
                    return "No value found";
                }
            }

            /**
             * Create a single <input> or <select> element suitable for the given parameter definition.
             * @param p the parameter as taken from the service metadata
             * @returns the form field
             */
            function createValueInput(p) {
                var input;
                if (p.parameter_type === 'http://w3id.org/meta-share/meta-share/boolean') {
                    // true/false select
                    input = $('<select>');
                    $('<option>false</option>').prop('selected', p.default_value === 'false').appendTo(input);
                    $('<option>true</option>').prop('selected', p.default_value !== 'false').appendTo(input);
                } else if (p.enumeration_value && p.enumeration_value.length) {
                    // enumerated parameter
                    input = $('<select>');
                    p.enumeration_value.sort(function(a, b) { return this_.collator(a.value_name, b.value_name); });
                    for(var j = 0; j < p.enumeration_value.length; j++) {
                        var val = p.enumeration_value[j];
                        var opt = $('<option>');
                        opt.attr('value', val.value_name);
                        opt.text(i(val.value_label));
                        opt.attr('title', i(val.value_description));
                        if(p.default_value && p.default_value == val.value_name) {
                            opt.prop('selected', true);
                        }
                        opt.appendTo(input);
                    }
                } else {
                    input = $('<input>').prop('required', true);
                    if (p.default_value) {
                        input.attr('value', p.default_value);
                    }
                    if (p.parameter_type === 'http://w3id.org/meta-share/meta-share/integer') {
                        input.attr('type', 'number').attr('step', 1);
                    } else if (p.parameter_type === 'http://w3id.org/meta-share/meta-share/float') {
                        input.attr('type', 'number').attr('step', 'any');
                    } else if (p.parameter_type === 'http://w3id.org/meta-share/meta-share/url1') {
                        input.attr('type', 'url');
                    } else {
                        // treat anything else as string
                        input.attr('type', 'text');
                    }
                }
                if (p.multi_value) {
                    input.attr('name', p.parameter_name + '[]')
                } else {
                    input.attr('name', p.parameter_name);
                }
                return input;
            }

            /**
             * Fix up the rows and buttons for a multi-valued parameter to match the given list of input fields.
             * @param section the tbody section for this parameter
             * @param inputs the correct list of value input elements, must not be empty
             */
            function fixUpMultiValue(section, inputs) {
                var rows = section.find('tr');
                var addButton = section.find('button.param-add');
                // delete trailing rows, if there are more rows than inputs
                rows.slice(inputs.length).remove();
                // add new rows if required
                for(var i = rows.length; i < inputs.length; i++) {
                    section.append($('<tr><td></td><td></td><td class="param-value"></td>' +
                        '<td><button class="mdc-icon-button material-icons param-delete" title="Delete this value">remove_circle</button></td>' +
                        '<td></td></tr>'));
                }
                // re-select rows including the new ones
                rows = section.find('tr');

                // put the input elements in the right rows
                var valueCells = section.find('td.param-value');
                for(var j = 0; j < inputs.length; j++) {
                    $(valueCells[j]).empty().append(inputs[j]);
                }

                // move the add button to the last row only
                addButton.appendTo(rows.last().find('td').last());
                // disable delete if there's only one row left
                rows.find('button.param-delete').prop('disabled', rows.length === 1);
            }

            var f = $('<form></form>');
            var tbl = $('<table></table>').appendTo(f);

            // event listeners
            // toggle disabled based on the "use this param" checkbox
            f.on('change', 'input.param-use', function(e) {
                var cb = $(this);
                var disable = !cb.prop('checked');
                var section = cb.closest('tbody');
                section.find('input, button, select').not('.param-use').prop('disabled', disable);
                // but make sure multi valued delete buttons are still disabled regardless of checkbox state
                // if there's only one value in the section
                var delButtons = section.find('button.param-delete');
                if(delButtons.length === 1) {
                    delButtons.prop('disabled', true);
                }
            });

            // handle the add/remove buttons for a multi-value parameter
            f.on('click', 'button.param-add', function(e) {
                e.preventDefault();
                var btn = $(this);
                var section = btn.closest('tbody');
                var inputs = section.find('td.param-value input').toArray();
                inputs.push(createValueInput(section.data('param')));
                fixUpMultiValue(section, inputs);
            });

            f.on('click', 'button.param-delete', function(e) {
                e.preventDefault();
                var btn = $(this);
                // delete this row's input element
                var row = btn.closest('tr');
                row.find('td.param-value input').remove();
                // gather the remaining inputs
                var section = row.closest('tbody');
                var inputs = section.find('td.param-value input').toArray();
                // and fix up
                fixUpMultiValue(section, inputs);
            });

            // add extra cells if ANY param is multi valued
            var hasMultiValuedParam = false;
            for(var n = 0; n < params.length; n++) {
                if(params[n].multi_value) {
                    hasMultiValuedParam = true;
                    break;
                }
            }

            var head = $('<thead><tr><th title="Should we send this parameter?" style="text-align: center">Use?</th><th>Name</th><th>Value</th></tr></thead>').appendTo(tbl);
            if(hasMultiValuedParam) {
                head.find('tr').append($('<th>&nbsp;</th><th>&nbsp;</th>'));
            }
            $.each(params, function(idx, p) {
                // make a separate tbody per param so we can have multiple rows for multi_value params
                var section = $('<tbody></tbody>').appendTo(tbl);
                section.data('param', p);
                var row = $('<tr></tr>').appendTo(section);
                var useCell = $('<td></td>').appendTo(row);
                useCell.append($('#param-checkbox-template').children().clone());
                new mdc.checkbox.MDCCheckbox(useCell.children()[0]);
                var useCheckbox = useCell.find('input');
                if(!p.optional) {
                    // required parameter - box is always checked
                    useCheckbox.prop('checked', true)
                        .prop('disabled', true);
                    useCell.children().attr('title', 'This parameter is required');
                }

                var nameCell = $('<td></td>').appendTo(row);
                nameCell.text(p.parameter_label ? i(p.parameter_label) : p.parameter_name);
                var valueCell = $('<td></td>').addClass('param-value').appendTo(row);
                if(p.parameter_description) {
                    var localizedDesc = i(p.parameter_description);
                    nameCell.attr('title', localizedDesc);
                    valueCell.attr('title', localizedDesc);
                }

                var valueInput = createValueInput(p);
                valueCell.append(valueInput);
                if(p.multi_value) {
                    // add button cells
                    $('<td><button class="mdc-icon-button material-icons param-delete" title="Delete this value">remove_circle</button></td>').appendTo(row);
                    $('<td><button class="mdc-icon-button material-icons param-add" title="Add another value">add_circle</button></td>').appendTo(row);
                    // then fix up the buttons
                    fixUpMultiValue(section, [valueInput]);
                } else if(hasMultiValuedParam) {
                    row.append($('<td>&nbsp;</td><td>&nbsp;</td>'))
                }

                useCheckbox.trigger('change');
            });

            return f;
        };


        ElgCommon.prototype.fetchParams = function (readyCallback) {
            var this_ = this;
            if (this_.serviceInfo.ApiRecordUrl) {
                $.get(this_.withAuthSettings({
                    url: this_.serviceInfo.ApiRecordUrl,
                    success: function (metadata, textStatus) {
                        if (metadata.described_entity &&
                            metadata.described_entity.lr_subclass &&
                            metadata.described_entity.lr_subclass.parameter &&
                            metadata.described_entity.lr_subclass.parameter.length) {
                            // this service takes parameters - create the form
                            var paramsForm = this_.buildParamsForm(metadata.described_entity.lr_subclass.parameter);

                            var paramsDiv = $('#service-params');
                            $('<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-10">')
                                .appendTo(paramsDiv.addClass('mdc-layout-grid__inner'))
                                .append('<h3 class="mdc-typography--subtitle2">Additional parameters</h3>')
                                .append(paramsForm);
                            paramsDiv.css('display', 'block');
                            this_.paramsForm = paramsForm;
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $('#service-params')
                            .append($('<div class="alert alert-warning"></div>')
                                .text("Failed to fetch metadata - calls may fail if the service requires parameters"))
                            .css('display', 'block');
                    },
                    complete: function () {
                        readyCallback();
                    }
                }));
            } else {
                // can't fetch parameter info, so we're ready now
                readyCallback();
            }
            this.fetchedParams = true;
        };

        function localResolveErrors(errors) {
            var placeholder = /{(\d+)}/g;
            var resolvedErrors = $.map(errors, function (e) {
                return e.text.replace(placeholder, function (match, num) {
                    return e.params[parseInt(num, 10)]
                });
            });
            return $.Deferred().resolve(resolvedErrors);
        }

        ElgCommon.prototype.resolveErrors = function (errors) {
            if (this.i18nUrl) {
                return $.post({
                    url: this.i18nUrl.href,
                    data: JSON.stringify(errors),
                    dataType: 'json',
                    contentType: 'application/json',
                }).fail(function (xhr, status, errorThrown) {
                    console.log(errorThrown);
                    return localResolveErrors(errors);
                });
            } else {
                return localResolveErrors(errors);
            }
        };

        ElgCommon.prototype.ajaxErrorHandler = function () {
            var this_ = this;
            return function (jqXHR, textStatus, errorThrown) {
                var errors = [];
                var responseJSON = jqXHR.responseJSON;
                var msgsContainer = $('#elg-messages');
                if (this_.submitProgress) {
                    this_.submitProgress.close();
                }
                if (responseJSON && responseJSON.hasOwnProperty("failure")) {
                    errors = responseJSON.failure.errors;
                    this_.resolveErrors(errors).then(function (data) {
                        $.each(data, function (i, msg) {
                            msgsContainer.append($('<div class="alert alert-warning"></div>').text(msg));
                        });
                        this_.afterErrorCallback();
                    });
                } else {
                    // this should be i18n'd too really
                    msgsContainer.append($('<div class="alert alert-warning">Unknown error occurred</div>'));
                    this_.afterErrorCallback();
                }
            }
        };


        ElgCommon.prototype.callService = function (data, contentType, responseHandler) {
            var errorHandler = this.ajaxErrorHandler();
            var submitProgress = this.submitProgress;
            var this_ = this;

            function pollForResult(pollUrl) {
                function doPoll() {
                    $.get(this_.withAuthSettings({
                        url: pollUrl,
                        success: function (respData, textStatus) {
                            if (respData.progress) {
                                if (submitProgress) {
                                    submitProgress.determinate = (respData.progress.percent > 0);
                                    submitProgress.progress = respData.progress.percent / 100.0;
                                }
                                // schedule the next poll
                                setTimeout(doPoll, 2000);
                            } else {
                                if (submitProgress) {
                                    submitProgress.close();
                                }
                                $('#process-state').text('\u00A0');
                                responseHandler(respData);
                            }

                            return false;
                        },
                        error: errorHandler
                    }));
                }

                setTimeout(doPoll, 2000);
            }

            var targetUrl = this_.serviceInfo.ServiceUrl;
            // add parameters if there are any
            if(this_.paramsForm) {
                if(this_.paramsForm[0].reportValidity()) {
                    var queryString = this_.paramsForm.serialize();
                    if (queryString) {
                        targetUrl += '?' + queryString;
                    }
                } else {
                    $('#elg-messages').append($('<div class="alert alert-warning">Service parameter settings are not valid</div>'));
                    this_.afterErrorCallback();
                    return;
                }
            }
            $.post(this_.withAuthSettings({
                url: targetUrl,
                data: data,
                success: function (respData, textStatus) {
                    if (respData.response && respData.response.type == 'stored') {
                        // async response, start polling
                        if (submitProgress) {
                            submitProgress.open();
                            submitProgress.determinate = false;
                            submitProgress.progress = 0;
                        }
                        $('#process-state').text('Processing');
                        var pollUrl = respData.response.uri;

                        // special case to support local testing with the service URLs proxied - rewrite the
                        // poll URL prefix to use the same proxy
                        if (this_.serviceInfo.guiIeRewriteFrom && pollUrl.startsWith(this_.serviceInfo.guiIeRewriteFrom)) {
                            pollUrl = this_.serviceInfo.guiIeRewriteTo + pollUrl.substring(this_.serviceInfo.guiIeRewriteFrom.length);
                        }
                        pollForResult(pollUrl);
                    } else {
                        if (submitProgress) {
                            submitProgress.close();
                        }
                        // sync response, handle it now
                        responseHandler(respData);
                    }
                    return false;
                },

                error: errorHandler,
                contentType: contentType,
                processData: false
            }));
        };

        return ElgCommon;
    })();
});
