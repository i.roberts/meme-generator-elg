// fix up directionality for the test form and annotation result section, e.g. if this
// gui is being used for Arabic NER then the form and the result should be rendered as RTL
(function() {
    var myUrl = new URL(window.location.href);
    if(myUrl.searchParams.has('dir')) {
        document.getElementById('elg-test-form').dir = myUrl.searchParams.get('dir');
        document.getElementById('elg-annotate-result').dir = myUrl.searchParams.get('dir');
    }
})();

require(["jquery", "mdc", "elg/common"], function ($, mdc, ElgCommon) {
    $(function () {
        mdc.autoInit();

        function enableSubmit() {
            $('#submit-form').prop('disabled', false);
        }

        var elgCommon = new ElgCommon(enableSubmit, enableSubmit,
            document.getElementById('submitprogress').MDCLinearProgress);

        $('#test-again').on('click', function(e) {
            e.preventDefault();
            $('#docView').empty();
            $('#elg-annotate-result').addClass('hidden');
            $('#elg-test-form').removeClass('hidden');
        });

        function handleResponse(data) {
            if (data.response
                && data.response.hasOwnProperty("features")
                && data.response.features.hasOwnProperty("imageUrl")) {

                $('#elg-test-form').addClass('hidden');
                $('#elg-annotate-result').removeClass('hidden');

                // re-enable the button
                $('#submit-form').prop('disabled', false);

                // show the image
                $('<img>').attr("src", data.response.features.imageUrl).appendTo("#docView");
            } else {
                var msgsContainer = $('#elg-messages');
                var error = {
                    code: 'elg.response.invalid',
                    text: 'Invalid response message'
                }

                elgCommon.resolveErrors([error]).then(function (resolvedErrors) {
                    $('<div class="alert alert-warning"></div>').text(resolvedErrors[0]).appendTo(msgsContainer);
                    // re-enable the button
                    $('#submit-form').prop('disabled', false);
                });
            }
        }

        $("#submit-form").on('click', function (e) {
            e.preventDefault();
            var topText = $('#top-text').val();
            var bottomText = $('#bottom-text').val();

            // disable the button until the REST call returns
            $('#submit-form').prop('disabled', true);
            $('#elg-messages').empty();

            elgCommon.callService(topText + "\n" + bottomText, "text/plain", handleResponse);
            return false;
        });
    });
});


