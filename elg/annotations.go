package elg

type AnnotationsResponseMessage struct {
	Response *AnnotationsResponse `json:"response,omitempty"`
	Failure  *Failure             `json:"failure,omitempty"`
}

type AnnotationsResponse struct {
	Type        string                  `json:"type"`
	Annotations map[string][]Annotation `json:"annotations"`
	Features    map[string]string       `json:"features,omitempty"`
}

type Annotation struct {
	Start    int64             `json:"start"`
	End      int64             `json:"end"`
	Features map[string]string `json:"features,omitempty"`
}
