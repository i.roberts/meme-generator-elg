package elg

type TextOrStructuredRequest struct {
	Type    string            `json:"type"`
	Content string            `json:"content,omitempty"`
	Texts   []Text            `json:"texts,omitempty"`
	Params  map[string]string `json:"params,omitempty"`
}

type Text struct {
	Content string `json:"content,omitempty"`
	Texts   []Text `json:"texts,omitempty"`
}
