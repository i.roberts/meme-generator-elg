package elg

type StatusMessage struct {
	Code   string                 `json:"code"`
	Text   string                 `json:"text"`
	Params []string               `json:"params,omitempty"`
	Detail map[string]interface{} `json:"detail,omitempty"`
}

type Failure struct {
	Errors []StatusMessage `json:"errors"`
}
