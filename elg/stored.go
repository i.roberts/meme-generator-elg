package elg

type StoredResponseMessage struct {
	Response *StoredResponse `json:"response,omitempty"`
	Failure  *Failure        `json:"failure,omitempty"`
}

type StoredResponse struct {
	Type string `json:"type"`
	URI  string `json:"uri"`
}
